/*
** EPITECH PROJECT, 2019
** my_compute_power_it
** File description:
**
*/

int my_compute_power_it(int nb , int p)
{
    int i;
    int rsl = 1;
    long int test = 1;

    if (p == 0)
        return (1);
    else if (p < 0)
        return (0);
    for (i = 0; i < p; i++)
    {
        test *= nb;
        if (test > 2147483647 || test < -2147483648)
            return (0);
    }
    for (i = 0; i < p; i++)
    {
        rsl *= nb;
    }
    return (rsl);
}
