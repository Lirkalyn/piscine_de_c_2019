/*
** EPITECH PROJECT, 2019
** my_compute_factorial_rec
** File description:
**
*/

int my_compute_factorial_rec(int nb)
{
    int rsl = nb;

    if (nb <= 0) {
        if (nb < 0)
            return (0);
        else
            return (1);
    }
    else if (nb == 1)
        return (nb);
    rsl *= my_compute_factorial_rec(nb - 1);
}
