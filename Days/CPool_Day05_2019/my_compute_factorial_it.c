/*
** EPITECH PROJECT, 2019
** my_compute_factorial_it
** File description:
**
*/

int my_compute_factorial_it (int nb)
{
    int i;
    int rsl = nb;

    if (nb < 0)
        return (0);
    else if (nb == 0)
        return (1);
    for (i = 1; i < nb; i++)
        rsl *= (nb - i);
    return (rsl);
}
