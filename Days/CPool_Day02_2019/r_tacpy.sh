#!/bin/bash

cut -d : -f 1 | awk 'NR % 2 == 0' | tac | rev | sort | tac | sed -n "$MY_LINE1","$MY_LINE2"p | sed -n '1{h};1!{H};${g;s/\n/, /pg}' | sed -e 's/$/&./g'
