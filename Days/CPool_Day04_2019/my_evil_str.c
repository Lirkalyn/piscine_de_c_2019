/*
** EPITECH PROJECT, 2019
** my_evil_str
** File description:
** 
*/

int my_strlen2(char const *str)
{
    int i;

    for (i = 0; str[i] != '\0'; i++);
    return (i);
}

char *my_evil_str(char *str)
{
    int i;
    int len;
    char tmp;

    len = my_strlen2(str);
    for (i = 0; i < (len / 2); i++) {
        tmp = str[i];
        str[i] = str[((len - 1) - i)];
        str[((len - 1) - i)] = tmp;
    }
    return (str);
}
