/*
** EPITECH PROJECT, 2019
** Assignment 1
** File description:
** 
*/

#include <unistd.h>

void square_display_up_down(int x)
{
    int i;

    for (i = 0; i < x; i++) {
        if (i == 0 || i == (x - 1))
            my_putchar('o');
        else
            my_putchar('-');
    }
    my_putchar('\n');
}

void square_display_inside(int x , int y)
{
    int i;
    int j;

    for (j = 0; j < y; j++) {
        for (i = 0; i < x; i++) {
            if (i == 0 || i == (x - 1))
                my_putchar('|');
            else
                my_putchar(' ');
        }
        my_putchar('\n');
    }
}

void rush(int x , int y)
{
    if (x <= 0 || y <= 0)
        write(2, "Invalid size\n", 13);
    else if (x == 1 && y == 1) {
        my_putchar('o');
        my_putchar('\n');
    }
    else {
        square_display_up_down(x);
        square_display_inside(x, (y - 2));
        if (y > 1)
            square_display_up_down(x);
    }
}
